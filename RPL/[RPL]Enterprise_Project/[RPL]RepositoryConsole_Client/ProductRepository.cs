﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPL_RepositoryConsole_Client {

    public class ProductRepository : Repository<Product>
    {
        public override Product Model { get; set; }
        public override Product Detail(dynamic result)
        {
            var entity = new Product()
            {
                IDictionary = result["id"].ToString() as string,
                Description = result["Description"].ToString() as string,
            };
            return entitiy;

        }

        public override string Query(Product entity = null)
        {
            var query = string.Empty;

            switch (verb)
            {
                case Databverbs.Get:
                    query = "";
                    break;
                case Dataverbs.Post:
                    query = string.Format("", entity.Id, entity.Description);
                    break;
                case Dataverbs.Put:
                    query = string.Format("", entity.Id, entity.Description);
                    break;
                case Dataverbs.Delete:
                    query = string.Format("", entity.Id);
                case Dataverbs.Generate:
                    query = "";
                    break;
            }
            return query;
        }
    }
}
